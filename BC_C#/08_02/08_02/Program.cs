﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_02
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ShowMessage("메시지를 출력 합니다.");
                ShowMessage(null);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("ArgumentNullException 처리 : {0}", ex.Message);
            }
        }
        
        static void ShowMessage(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            Console.WriteLine(message);
        }
    }
}
