﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_04
{
    class Program
    {
        public class Product
        {
            public const int ConstPrice = 1000;
            public readonly int ReadOnlyPrice;

            public Product()
            {
                this.ReadOnlyPrice = 2000;
            }
            public Product(int price)
            {
                this.ReadOnlyPrice = ReadOnlyPrice;

            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("ConstPrice={0}", Product.ConstPrice);

            Product item1 = new Product();
            Console.WriteLine("new Product() : ReadOnlyPrice={0}", item1.ReadOnlyPrice);

            Product item2 = new Product(3000);
            Console.WriteLine("new Product(3000) : ReadOnlyPrice={0}", item2.ReadOnlyPrice);
        }
    }
}
