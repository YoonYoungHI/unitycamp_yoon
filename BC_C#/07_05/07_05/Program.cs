﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_05
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;

            for (int i = 1; 1<= 100; i++)
            {
                if (i> 10)
                {
                    break;
                }
                if ((i%2)==0)
                {
                    continue;
                }
                result += i;
            }

            Console.WriteLine("break-continue 1~10 중 홀수만 더하기: {0}", result);
        }
    }
}
