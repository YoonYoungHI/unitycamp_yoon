﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_02
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;
            int[] number = new int[10];

            for (int i = 0; i <10; i++)
            {
                number[i] = i + 1;
            }
              
            foreach (int i in number)
            {
                result += i;
            }

            Console.WriteLine("foreach 문 1~10 더하기 : {0}", result);
        }
    }
}
