﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_01
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array1D = { 1, 2, 3, 4, 5 };
            Console.WriteLine("Rank:{0}, Length:{1}", array1D.Rank, array1D.Length);
            Console.WriteLine("1 차원 배열 - foreach");
            foreach (int value in array1D)
                {
                Console.WriteLine(value);
            }

            Console.WriteLine("1 차원 배열 - for");
            for (int i = 0; i < array1D.Length; i++)
            {
                Console.WriteLine("array[{0}] : {1}", i, array1D[i]);
            }
        }
    }
}