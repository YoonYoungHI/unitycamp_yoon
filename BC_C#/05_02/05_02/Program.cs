﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_02
{
    public class Employee
    {
        public int BirthYear;
        public string Name;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee();
            emp1.Name = "김도현";
            emp1.BirthYear = 1983;

            Employee emp2 = emp1;

            Console.WriteLine("emp1.BirthYear : {0}", emp1.BirthYear);
            Console.WriteLine("emp2.BirthYear : {0}", emp2.BirthYear);

            emp1.BirthYear = 1978;
            Console.WriteLine("===emp1.BirthYear = 1978 값 변경 ===");
            Console.WriteLine("emp1.BirthYear : {0}", emp1.BirthYear);
            Console.WriteLine("emp2.BirthYear : {0}", emp2.BirthYear);
        }
    }
}
