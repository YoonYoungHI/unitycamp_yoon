﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_05
{
    class Program
    {
        static void Main(string[] args)
        {
            string numberString = "123";
            int number1 = Convert.ToInt32(numberString);

            int number2 = int.Parse(numberString);

            Console.WriteLine("number1 : {0}", number1);
            Console.WriteLine("number2 : {0}", number2);
        }
    }
}
