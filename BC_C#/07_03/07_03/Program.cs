﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_03
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;

            int startNumber = 1;
            int maxValue = 10;

            while (startNumber <=maxValue)
            {
                result += startNumber;
                startNumber++;
            }

            Console.WriteLine("while문 1~10 더하기 : {0}", result);
        }
    }
}
