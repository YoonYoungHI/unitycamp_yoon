﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_02
{
    public enum Country { Korea, China, Japan };

    class Program
    {
        static void Main(string[] args)
        {
            Country myCountry = Country.Korea;

            switch (myCountry)
                {
                case Country.Korea:
                Console.WriteLine("한국");
                break;
                case Country.China:
                Console.WriteLine("중국");
                    break;
                case Country.Japan:
                Console.WriteLine("일본");
                break;
                default:
                Console.WriteLine("선택된 나라가 없습니다.");
                break;
            }
        }
    }
}
