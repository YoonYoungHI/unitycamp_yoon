﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_01
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;

            for (int i = 1; i <=10; i++)
            {
                result += i;
            }

            Console.WriteLine("for문 1~10더하기 : {0}", result);
        }
    }
}
