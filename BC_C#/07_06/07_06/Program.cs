﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;

            for (int i = 1; i <= 10; i++)
            {
                result += i;

                if (i == 5)
                {
                    goto Jump;
}
            }
            Console.WriteLine("for문 1~10 더하기 : {0}", result);

        Jump:
            Console.WriteLine("goto 점프 : {0}, result");
        }
    }
}
