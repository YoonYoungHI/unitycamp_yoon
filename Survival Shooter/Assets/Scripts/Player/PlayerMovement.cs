﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public float speed = 6f; //속도컨트롤

	Vector3 movement;
	Animator anim;
	Rigidbody playerRigidbody;
	int floorMask;
	float camRayLength = 100f;

	void Avake()
	{
		floorMask = LayerMask.GetMask ("Floor");
		anim = GetComponent <Animator> ();
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate()
	{
		float h = Input.GetAxisRaw ("Horizontal");
		//서서히가속아니고 바로 최고속도
		//A나 D로 기본적으로 설정되어있다
		float V = Input.GetAxisRaw ("vertical");

		Move (h, V);
		Turning ();
		Animating (h, V);

	}

	void Move (float h, float v)
	{
		movement.Set (h, 0f, v); //바닥과 수평 바닥위 횡적이동

		movement = movement.normalized * speed * Time.deltaTime;
		//항상 같은 속도로 이동
		//대각선 이동 유리하지않게
		//deltaTime은 각 업데이트 호출의 시간 간격

		playerRigidbody.MovePosition (transform.position + movement);
	}

	void Turning()
	{
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit floorHit;
		if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) 
		{
			Vector3 playerToMouse = floorHit.point - transform.position;
			playerToMouse.y = 0f;

			Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
			playerRigidbody.MoveRotation (newRotation);
		}
	}

	void Animating(float h, float v)
	{
		bool walking = h != 0f || v != 0f;
		//수평축인가 수직축인가
		anim.SetBool("IsWalking",walking);

	}
}

